import java.util.Scanner;
public class Fallunterscheidungen_aufgabe4 {
	public static void main(String[]arrgs) {
		Scanner eingabe = new Scanner(System.in);
		
		System.out.print("erste Zahl: ");
		double zahl1 = eingabe.nextDouble();
		System.out.print("zweite Zahl: ");
		double zahl2 = eingabe.nextDouble();
		System.out.print("Rechenart (+, -, *, /: ");
		char rechen = eingabe.next().charAt(0);
		
		double ergebnis = 0;
		
		switch (rechen)
		{
			case '+':
				ergebnis = zahl1 + zahl2;
				break;
			case '-':
				ergebnis = zahl1 - zahl2;
				break;
			case '*':
				ergebnis = zahl1 * zahl2;
				break;
			case '/':
				ergebnis = zahl1 / zahl2;
				break;
			default:
				System.out.print("Fehler!");
				break;
		}
		
		System.out.print("Ergebnis: " + ergebnis);
		
	}
}
