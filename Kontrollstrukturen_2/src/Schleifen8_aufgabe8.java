import java.util.Scanner;

public class Schleifen8_aufgabe8 {

	public static void main(String[] args) {
		int zahl1 = zahlenEingabe();
		int zahl2 = zahlenEingabe();
		ausgabeMatrix(zahl1, zahl2);
	}

	
	public static int zahlenEingabe() {
		boolean pruefWert = true;
		int minimum = 1;
		int maximum = 10;
		int nutzerZahl = 0;
		while(pruefWert) {
			System.out.print("Bitte eine Zahl zwischen 2 und 9 eingeben: ");
			Scanner nutzerEingabe = new Scanner(System.in);
			nutzerZahl = nutzerEingabe.nextInt();
			if(nutzerZahl > minimum && nutzerZahl < maximum) {
				pruefWert = false;
			}
			else {
				System.out.println("Sie haben eine falsche Eingabe get�tigt.");
			}
		}
		return nutzerZahl;
	}
	
	
	public static void ausgabeMatrix(int zahlEins, int zahlZwei) {
		int min = 1;
		int max = 10;
		int temp = 0;
		for(int zeile = min; zeile <= max; zeile ++) {
			for(int spalte = min; spalte <= max; spalte ++) {
				temp = (((zeile-1)*10)+(spalte-1));
				int stelleEins = temp % 10;
				int stelleZwei = (temp/10) % 10;
				int quersumme = stelleEins + stelleZwei;
				if(temp == 0) {
					System.out.printf("%5d", temp);
				}
				else if(stelleEins == zahlEins || stelleEins == zahlZwei || stelleZwei == zahlEins || stelleZwei == zahlZwei || temp % zahlEins == 0 || temp % zahlZwei == 0 || quersumme == zahlEins || quersumme == zahlZwei) {
					System.out.printf("%5s", "*");
				}
				else {
					System.out.printf("%5d", temp);	
				}
			}
			System.out.println();
		}
	}	
}

