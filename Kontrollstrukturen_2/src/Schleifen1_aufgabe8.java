import java.util.Scanner;
public class Schleifen1_aufgabe8 {
	public static void main(String[]args) {
		Scanner eingabe = new Scanner(System.in);
		
		System.out.print("Geben sie eine Seitenlšnge ein: ");
		int laenge = eingabe.nextInt();
		
		for (int zeile = 0; zeile <= laenge; zeile++) {
			for (int spalte = 0; spalte <= laenge; spalte++) {
				if(zeile == 0 || zeile ==laenge) {
					System.out.printf("%2s","*");
				}
				if(zeile != 0 && zeile != laenge) {
					if(spalte == 0 || spalte == laenge) {
						System.out.printf("%2s","*");
					}
					else {
						System.out.printf("%2s"," ");
					}
				}
			}
			System.out.println();
		}
		
		
		
		
	}

}
