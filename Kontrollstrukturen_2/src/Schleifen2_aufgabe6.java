import java.util.Scanner;
public class Schleifen2_aufgabe6 {

	public static void main(String[] args) {
		//VARIABLENDEKLARATION
		boolean endlos = true;
		boolean pruefung1 = true;
		boolean pruefung2 = true;
		boolean pruefung3 = true;
		double einlagenHoehe = 0.0;
		double zinssatz = 0.0;
		int jahre = 1;
		double temp = 0.0;
		double grenzkapital = 1000000.0;
		double endkapital = 0.0;
		char nutzerEntscheidung = 'a';

		
		while(pruefung3) {
			while(endlos) {
				while(pruefung1) {
					Scanner nutzerEingabe = new Scanner(System.in);
					System.out.print("Bitte die H�he Ihrer Einlage eingeben: ");
					einlagenHoehe = nutzerEingabe.nextDouble();
					System.out.print("Bitte die H�he Ihres Zinssatzes eingeben: ");
					zinssatz = nutzerEingabe.nextDouble();
					if(einlagenHoehe > 0) {
						pruefung1 = false;
					}
					else {
						System.out.println("Sie haben eine falsche Eingabe vorgenommen");
					}
				}
			
				while(pruefung2) {
					temp = einlagenHoehe + (1*(zinssatz/100));
					endkapital = endkapital + temp;
					if(endkapital > grenzkapital) {
						pruefung2 = false;
					}
					else {
						jahre += 1;
					}
				}
			
				System.out.println("Bei einer Einlagenh�he von " + einlagenHoehe + " und einem Zinssatz von " + zinssatz + " w�rden Sie " + jahre + " Jahre ben�tigen um 1 Mio EUR zu sparen.");
			
				
					System.out.print("Wollen Sie die Bearbeitung fortsetzen? (j/n): ");
					Scanner entscheidungScanner = new Scanner(System.in);
					nutzerEntscheidung = entscheidungScanner.next().charAt(0);
					if(nutzerEntscheidung == 'j') {
						System.out.println("Das Programm wird erneut ausgef�hrt.");			
						pruefung3 = true;
					}
					else if(nutzerEntscheidung == 'n') {
						System.out.println("Das Programm wird beendet.");
						pruefung3 = false;
					}
					else {
						System.out.println("Sie haben eine falsche Eingabe get�tigt.");
					}	
				}
			}	
		}	
	}

