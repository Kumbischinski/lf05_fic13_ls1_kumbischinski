﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static double fahrkartenbestellungErfassen() {
		Scanner eingabe = new Scanner(System.in);  
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		   System.out.println("(1) Einzelfahrschein Regeltarif AB [2.90 EUR]");
		   System.out.println("(2) Tageskarte Regeltarif AB [8,60 EUR]");
		   System.out.println("(3) Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]");
		   int auswahl = eingabe.nextInt();
		   
		   double zuZahlenderBetrag = 0.0;
		   byte anzahlTickets = 0;
		   
		   switch(auswahl) {
		   case 1:
			   System.out.println("Zu zahlender Betrag (EURO): 2,90");
		       zuZahlenderBetrag = 2.90;
		       System.out.print("\nBitte die Anzahl benötigter Tickets eingeben: ");
		       anzahlTickets = eingabe.nextByte();
		       zuZahlenderBetrag = anzahlTickets * zuZahlenderBetrag;
			   break;
		   case 2:
			   System.out.println("Zu zahlender Betrag (EURO): 8,60");
		       zuZahlenderBetrag = 8.60;
		       System.out.print("\nBitte die Anzahl benötigter Tickets eingeben: ");
		       anzahlTickets = eingabe.nextByte();
		       zuZahlenderBetrag = anzahlTickets * zuZahlenderBetrag;
			   break;
		   case 3:
			   System.out.println("Zu zahlender Betrag (EURO): 23,50");
		       zuZahlenderBetrag = 23.50;
		       System.out.print("\nBitte die Anzahl benötigter Tickets eingeben: ");
		       anzahlTickets = eingabe.nextByte();
		       zuZahlenderBetrag = anzahlTickets * zuZahlenderBetrag;
			   break;
		   default:
			   System.out.println("Es wurde eine falsche Auswahl getroffen.");
		   }
	       return zuZahlenderBetrag;
    }
	
	 public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		 Scanner tastatur = new Scanner(System.in);
		 double eingeworfeneMuenze;
		 double eingezahlterGesamtbetrag = 0.0;
	     while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMuenze = tastatur.nextDouble();
	           if (eingeworfeneMuenze > 0 || eingeworfeneMuenze <= 2) {
	    	   eingezahlterGesamtbetrag += eingeworfeneMuenze;}
	           else {
	        	   System.out.println("Falscher einwurf!");}
	       }
  	     return eingezahlterGesamtbetrag;
     }
	 
     public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
     }
     
     public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	 double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
         if(rückgabebetrag > 0.0)
         {
      	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
             {
          	  System.out.println("2 EURO");
  	          rückgabebetrag -= 2.0;
             }
             while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
             {
          	  System.out.println("1 EURO");
  	          rückgabebetrag -= 1.0;
             }
             while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
             {
          	  System.out.println("50 CENT");
  	          rückgabebetrag -= 0.5;
             }
             while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
             {
          	  System.out.println("20 CENT");
   	          rückgabebetrag -= 0.2;
             }
             while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
             {
          	  System.out.println("10 CENT");
  	          rückgabebetrag -= 0.1;
             }
             while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
             {
          	  System.out.println("5 CENT");
   	          rückgabebetrag -= 0.05;
             }
         }

         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir wünschen Ihnen eine gute Fahrt.");
      }
     
     public static void main(String[] args)
     {            
 		boolean endlos = true;
 		while(endlos) {
 			boolean pruefung = true;
 	    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
 	    	double eingezahlterBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
 	    	fahrkartenAusgeben();
 	    	rueckgeldAusgeben(eingezahlterBetrag, zuZahlenderBetrag);
 	    	
 	    	while(pruefung) {
 		    	System.out.println();
 		    	Scanner nutzerEingabe = new Scanner(System.in);
 		    	System.out.print("Möchten Sie einen weiteren Kauf tätigen? (J/N): ");
 		    	char nutzerAuswahl = nutzerEingabe.next().charAt(0);
 		    	nutzerAuswahl = java.lang.Character.toUpperCase(nutzerAuswahl);
 		    	
 		    	if(nutzerAuswahl == 'N') {
 		    		System.out.println();
 		    		System.out.println("Wir wünschen Ihnen eine gute Fahrt.");
 		    		System.out.println();
 		    		endlos = false;
 		    		pruefung = false;
 		    		nutzerEingabe.close();
 		    	}
 		    	else if(nutzerAuswahl == 'J') {
 		    		System.out.println();
 		    		System.out.println("Kaufvorgang wird neu gestartet.");
 		    		System.out.println();
 		    		pruefung = false;
 		    	}
 		    	else {
 		    		System.out.println();
 		    		System.out.println("Es wurde eine falsche Eingabe getätigt.");
 		    		System.out.println();
 		    	}	
 	    	}
 		}
     }
}
